import UltravioletLocalDB from "app/data/database/UltravioletLocalDB";
import UltravioletStore from "app/data/UltravioletStore";

export class RecentPages {
    static get recentPages(): typeof UltravioletLocalDB.i.recentPages {
        return UltravioletLocalDB.i.recentPages;
    }
    private static get page(): typeof UltravioletStore.currentPage {
        return UltravioletStore.currentPage;
    }

    static pages: string[];

    static async init(): Promise<void> {
        RecentPages.recentPages.put({
            title: RecentPages.page.title.getPrefixedText(),
            lastVisit: Date.now(),
        });

        (await RecentPages.recentPages.getAll())
            .sort((a, b) => b.lastVisit - a.lastVisit)
            .slice(50)
            .forEach((page) => RecentPages.recentPages.delete(page.title));

        RecentPages.pages = (await RecentPages.recentPages.getAll())
            .sort((a, b) => b.lastVisit - a.lastVisit)
            .map((page) => page.title);
    }
}
