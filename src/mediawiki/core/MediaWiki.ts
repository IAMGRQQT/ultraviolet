// Use as little imports as possible.
import i18next from "i18next";
import semanticDifference from "app/util/semanticDifference";
import Log from "app/data/AppLog";

interface MediaWikiCheck {
    silent?: boolean;
    test(): boolean;
}

const mwChecks: Record<string, MediaWikiCheck> = {
    version: {
        test: () => {
            // Support for `wgDiffOldId` and `wgDiffNewId`
            return (
                semanticDifference(mw.config.get("wgVersion"), "1.30.0") !== -1
            );
        },
    },
    user: {
        test: () => {
            return mw.user.getName() !== null;
        },
    },
    // skin: {
    //     silent: true,
    //     test: () => {
    //         // Minerva is unsupported as of now. Disable.
    //         return mw.config.get("skin") !== "minerva";
    //     },
    // },
};

/**
 * The MediaWiki class is responsible for checks and actions against the MediaWiki
 * JavaScript interface, the primary interface allowing wiki-to-userscript interaction.
 */
export class MediaWiki {
    /**
     * Run all MediaWiki checks and return whichever fails.
     */
    static runMWChecks(): string[] {
        const failedChecks: string[] = [];

        for (const check of Object.keys(mwChecks)) {
            if (!mwChecks[check].test()) failedChecks.push(check);
        }

        return failedChecks;
    }

    /**
     * Load in every MediaWiki dependency Ultraviolet needs.
     */
    static async loadDependencies() {
        await mw.loader.using([
            "mediawiki.api",
            "mediawiki.util",
            "mediawiki.Title",
            "mediawiki.language.months",
        ]);
    }

    /**
     * Run all MediaWiki checks and show notifications if checks are failed.
     * @returns Whether or not checks passed.
     */
    static mwCheck(): boolean {
        const checks = MediaWiki.runMWChecks();

        if (checks.length > 0) {
            const notification = document.createElement("div");
            notification.innerText = i18next.t("common:ultraviolet.init.error");

            const list = document.createElement("ul");
            for (const failedCheck of checks) {
                Log.fatal("MediaWiki check failed: " + failedCheck);

                const listItem = document.createElement("li");
                listItem.innerText = i18next.t(
                    `common:ultraviolet.init.mwChecks.${failedCheck}`
                );

                if (!mwChecks[failedCheck].silent) list.appendChild(listItem);
            }
            notification.appendChild(list);

            if (list.childElementCount > 0) mw.notify(notification);

            return false;
        } else return true;
    }
}
