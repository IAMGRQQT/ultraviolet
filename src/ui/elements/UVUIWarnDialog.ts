import {
    Page,
    RevertContext,
    User,
    Warning,
    WarningOptions,
} from "app/mediawiki";
import { UVUIDialog, UVUIDialogProperties } from "app/ui/elements/UVUIDialog";

export interface UVUIWarnDialogProps extends UVUIDialogProperties {
    rollbackContext?: RevertContext;
    targetUser?: User;
    defaultWarnReason?: Warning;
    defaultWarnLevel?: number;
    relatedPage?: Page;
}

export class UVUIWarnDialog extends UVUIDialog<WarningOptions | null> {
    show(): Promise<WarningOptions> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvWarnDialog";

    constructor(readonly props: UVUIWarnDialogProps) {
        super(props);
    }
}
