import { Dependency } from "app/data/Dependencies";
import {
    UVUIDialog,
    UVUIDialogAction,
    UVUIDialogProperties,
} from "app/ui/elements/UVUIDialog";

export interface UVUIIFrameDialogProps extends UVUIDialogProperties {
    /**
     * The height of the dialog in whatever CSS unit specified.
     */
    height?: string;
    src: string;
    fragment?: string;
    dependencies?: Dependency[];
    customStyle?: string | string[];
    customScripts?: string | string[];
    actions?: UVUIDialogAction[];

    /**
     * Whether or not to disable Ultraviolet in the IFrame. This is intended
     * for events where a MediaWiki page of the same wiki is loaded, causing
     * the Ultraviolet userscript to load as well.
     *
     * This should always append the `uv-disable` class onto the body.
     */
    disableUltraviolet?: boolean;
}

export class UVUIIFrameDialog extends UVUIDialog<void> {
    show(): Promise<any> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvIFrameDialog";

    constructor(readonly props: UVUIIFrameDialogProps) {
        super(props);
    }
}
