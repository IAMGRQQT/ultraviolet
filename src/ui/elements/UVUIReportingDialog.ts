import type { Page } from "app/mediawiki";
import { User } from "app/mediawiki";
import type { ReportVenue } from "app/mediawiki/report/ReportVenue";
import { ReportVenueMode } from "app/mediawiki/report/ReportVenue";
import { UVUIDialog, UVUIDialogProperties } from "app/ui/elements/UVUIDialog";
import { Report } from "app/mediawiki/report/Report";

export type UVUIReportingDialogTargetType<T extends ReportVenue> =
    T["mode"] extends ReportVenueMode.User ? User : Page;
export interface UVUIReportingDialogProps extends UVUIDialogProperties {
    venue: ReportVenue;
    notice?: string;
    target?: User | Page;
}

export class UVUIReportingDialog extends UVUIDialog<Report> {
    show(): Promise<Report> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvReportingDialog";

    constructor(readonly props: UVUIReportingDialogProps) {
        super(props);
    }
}
