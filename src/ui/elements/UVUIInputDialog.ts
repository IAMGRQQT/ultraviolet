import {
    OKCancelActions,
    UVUIDialog,
    UVUIDialogProperties,
} from "app/ui/elements/UVUIDialog";
import { UVUITextInputProperties } from "app/ui/elements/UVUITextInput";

export interface UVUIInputDialogProps
    extends UVUIDialogProperties,
        UVUITextInputProperties {
    /**
     * The actions of the dialog. These go at the bottom of the dialog.
     */
    actions?: OKCancelActions;
    /**
     * Set to `true` if the OK button should be emphasized.
     */
    progressive?: boolean;
}

export class UVUIInputDialog extends UVUIDialog<string> {
    show(): Promise<string> {
        throw new Error("Attempted to call abstract method");
    }
    render(): HTMLDialogElement {
        throw new Error("Attempted to call abstract method");
    }

    public static readonly elementName = "uvInputDialog";

    constructor(readonly props: UVUIInputDialogProps) {
        super(props);
    }
}
